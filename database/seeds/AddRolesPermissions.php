<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Model\Admin\Auth\User;

class AddRolesPermissions extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->cleanDB();
        $this->createAdminUser();
        $this->addRoles();

    }

    protected function cleanDB()
    {
        DB::table('users')->delete();
        DB::table('roles')->delete();
        DB::table('role_has_permissions')->delete();
        DB::table('permissions')->delete();
        DB::table('model_has_permissions')->delete();
        DB::table('model_has_roles')->delete();
    }

    protected function createAdminUser()
    {
        DB::insert(
            "INSERT INTO `test-task`.users (id, name, email, email_verified_at, password, remember_token, created_at, updated_at) 
            VALUES (7, 'Jon', 'a@a.c', null, '$2y$10\$qjYc5OOmq9BbSW1y1hUuUeqk9DYN4QuAtnDvqtW0tEBkORCA0JuUa', null, '2020-03-18 15:36:37', '2020-03-18 15:36:37')"
        );
    }

    protected function addRoles()
    {
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        $permissionSuffixes = [
            'cars', 'dashboard', 'users', 'bookings', 'clients'
        ];

        $permissionPrefixes = [
            'view',
            'edit',
            'add',
            'delete'
        ];

        foreach ($permissionPrefixes as $prefix){
            foreach ( $permissionSuffixes as $suffix){
                Permission::create(['name' => $prefix . '_' . $suffix]);
            }
        }

        Role::create(['name' => 'admin'])
            ->givePermissionTo(Permission::all());

        $readPermissions = [];

        foreach ($permissionSuffixes as $suffix)
        {
            $readPermissions[] = 'view_'.$suffix;
        }

        Role::create(['name' => 'manager'])
            ->givePermissionTo($readPermissions);

        $user = User::where('email', '=', 'a@a.c')->firstOrFail();
        $user->assignRole('admin');
    }
}
