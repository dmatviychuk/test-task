<?php

namespace App\Http\Controllers\Frontend;

use App\Model\Common\Car\Car;
use App\Http\Controllers\Controller;


class HomeController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    protected const PER_PAGE_LIMIT = 10;

    public function index()
    {
        $cars = Car::paginate(static::PER_PAGE_LIMIT);

        return view(
            'frontend.home.index',
            compact('cars')
        );
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function about()
    {
        return view('frontend.home.about');
    }
}
