<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Model\Common\Car\Car;
use App\Http\Controllers\Controller;
use App\Model\Common\{BookingStatus,Client, Booking};


class BookingController extends Controller
{
    public function create($carId)
    {
        $car = Car::findOrFail($carId);
        return view('frontend.booking.create', compact('car'));
    }

    public function store(Request $request)
    {
        $request->validate(Client::$storeWithBooking);
        $request->validate(Booking::$storeRules);

        if (($client = Client::where('phone', '=', $request->get('phone'))->get()->First()) === null) {
            $client = new Client([
                'name' => $request->get('name'),
                'phone' => $request->get('phone')
            ]);
            $client->save();
        }

        Car::findOrFail($request->get('car_id'));

        $booking = new Booking([
            'client_id' => $client->id,
            'start_date' => $request->get('start_date'),
            'end_date' => $request->get('end_date'),
            'status' => BookingStatus::NEW,
            'car_id' => $request->get('car_id')
        ]);

        $booking->save();

        return redirect()
            ->route('booking.success');
    }

    public function success()
    {
        return view('frontend.booking.success');
    }
}

