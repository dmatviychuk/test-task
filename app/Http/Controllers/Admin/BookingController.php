<?php

namespace App\Http\Controllers\Admin;

use App\Model\Common\Booking;
use Illuminate\Http\Request;
use App\Model\Common\BookingStatus;

class BookingController extends AdminBaseController
{

    public const PER_PAGE_LIMIT = 10;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bookings = Booking::paginate(static::PER_PAGE_LIMIT);

        return view(
            'admin.bookings.index',
            compact('bookings')
        );
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Model\Common\Booking $booking
     * @return \Illuminate\Http\Response
     */
    public function show(Booking $booking)
    {
        return view(
            'admin.bookings.show',
            compact('booking')
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Model\Common\Booking $booking
     * @return \Illuminate\Http\Response
     */
    public function edit(Booking $booking)
    {
        $statuses = BookingStatus::getPossibleStatuses();
        return view('admin.bookings.edit', compact('booking', 'statuses'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Model\Common\Booking $booking
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Booking $booking)
    {
        $booking->status = $request->get('status');
        $booking->save();

        return redirect()
            ->route('bookings.index')
            ->with('flash.message', 'Booking item updated!')
            ->with('flash.class', 'success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Model\Common\Booking $booking
     * @return \Illuminate\Http\Response
     */
    public function destroy(Booking $booking)
    {
        $booking->delete();

        return redirect()
            ->route('bookings.index')
            ->with('flash.message', 'Booking item deleted!')
            ->with('flash.class', 'success');
    }
}
