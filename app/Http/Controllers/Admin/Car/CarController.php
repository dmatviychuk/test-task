<?php

namespace App\Http\Controllers\Admin\Car;

use Illuminate\Http\Request;
use App\Model\Common\Car\Car;
use App\Http\Controllers\Admin\AdminBaseController;


class CarController extends AdminBaseController
{
    protected const PER_PAGE_LIMIT = 10;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cars = Car::paginate(static::PER_PAGE_LIMIT);

        return view(
            'admin.cars.index',
            compact('cars')
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.cars.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(Car::$storeRules);

        $car = new Car($request->except('image'));
        $car->setCarImage($request->file('image'));
        $car->save();

        return redirect()
            ->route('cars.index')
            ->with('flash.message', 'Car item created!')
            ->with('flash.class', 'success');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Admin\Car\Car  $car
     * @return \Illuminate\Http\Response
     */
    public function show(Car $car)
    {
        return view(
            'admin.cars.show',
            compact('car')
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Admin\Car\Car  $car
     * @return \Illuminate\Http\Response
     */
    public function edit(Car $car)
    {
        return view(
            'admin.cars.edit',
            compact('car')
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Admin\Car\Car  $car
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Car $car)
    {
        $request->validate(Car::$updateRules);

        $car->title = $request->get('title');
        $car->year = $request->get('year');
        $car->rent_price = $request->get('rent_price');
        $car->color = $request->get('color');
        $car->interior_color = $request->get('interior_color');

        if(($image =$request->file('image')) !== null) {
            $car->setCarImage($image);
        }

        $car->save();

        return redirect()
            ->route('cars.index')
            ->with('flash.message', 'Car item updated!')
            ->with('flash.class', 'success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Admin\Car\Car  $car
     * @return \Illuminate\Http\Response
     */
    public function destroy(Car $car)
    {
        $car->delete();

        return redirect()
            ->route('cars.index')
            ->with('flash.message', 'Car item deleted!')
            ->with('flash.class', 'success');
    }
}