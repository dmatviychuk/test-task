<?php

namespace App\Http\Controllers\Admin;


class HomeController extends AdminBaseController
{
    public function index()
    {
        return view('admin.home.index');
    }
}
