<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Admin\Auth\Authorizable;

class AdminBaseController extends Controller
{
    use Authorizable;
}
