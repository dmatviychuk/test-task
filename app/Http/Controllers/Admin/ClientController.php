<?php

namespace App\Http\Controllers\Admin;

use App\Model\Common\Client;
use Illuminate\Http\Request;


class ClientController extends AdminBaseController
{

    public const PER_PAGE_LIMIT = 10;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clients = Client::paginate(static::PER_PAGE_LIMIT);

        return view(
            'admin.clients.index',
            compact('clients')
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.clients.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(Client::$storeRules);
        (new Client($request->all()))->save();

        return redirect()
            ->route('clients.index')
            ->with('flash.message', 'Client item created!')
            ->with('flash.class', 'success');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\app\Model\Common\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function show(Client $client)
    {
        return view('admin.clients.show', compact('client'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\app\Model\Common\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function edit(Client $client)
    {
        return view('admin.clients.edit', compact('client'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\app\Model\Common\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Client $client)
    {
        $client->setRawAttributes(
            $request->validate(Client::$storeRules)
        )->save();

        return redirect()
            ->route('clients.index')
            ->with('flash.message', 'Client item updated!')
            ->with('flash.class', 'success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\app\Model\Common\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function destroy(Client $client)
    {
        $client->delete();

        return redirect()
            ->route('clients.index')
            ->with('flash.message', 'Client item deleted!')
            ->with('flash.class', 'success');
    }
}
