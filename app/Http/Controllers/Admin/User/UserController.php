<?php

namespace App\Http\Controllers\Admin\User;

use App\Model\Admin\Auth\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Role;
use App\Http\Controllers\Admin\AdminBaseController;


class UserController extends AdminBaseController
{
    protected const PER_PAGE_LIMIT = 10;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::paginate(static::PER_PAGE_LIMIT);

        return view(
            'admin.users.index',
            compact('users')
        );
    }


    /**
     * Display the specified resource.
     *
     * @param \App\Model\Admin\Auth\User $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return view(
            'admin.users.show',
            compact('user')
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Model\Admin\Auth\User $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $roles = Role::all();

        return view(
            'admin.users.edit',
            compact('user', 'roles')
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Model\Admin\Auth\User $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $newRoleId = $request->get('roleId');
        $currentRoleId = $user->roles->first()->id;

        if ($newRoleId != $currentRoleId) {
            $user->roles()->detach();
            $user->roles()->attach($newRoleId);
        }

        return redirect()
            ->route('users.index')
            ->with('flash.message', 'User updated!')
            ->with('flash.class', 'success');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Model\Admin\Auth\User $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $flashMessage = 'User account deleted!';
        $status = 'success';

        if (Auth::user()->id === $user->id) {
            $flashMessage = 'You cant delete you current active user!';
            $status = 'warning';
        }else {
            $user->delete();
        }

        return redirect()
            ->route('users.index')
            ->with('flash.message', $flashMessage)
            ->with('flash.class', $status);

    }
}
