<?php

namespace App\Model\Common;

use Illuminate\Database\Eloquent\Model;


class Client extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'name',
        'phone',
    ];

    public static $storeRules = [
        'name'  => 'required|max:30',
        'phone' => 'required|max:30|unique:clients',
    ];

    public static $storeWithBooking = [
        'name'  => 'required|max:30',
        'phone' => 'required|max:30',
    ];

    public static function boot()
    {
        parent::boot();
        Client::deleted(function ($client) {
            Booking::whereIn('client_id', [$client->id])->delete();
        });
    }
}


