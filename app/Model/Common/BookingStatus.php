<?php

namespace App\Model\Common;


class BookingStatus
{
    public const NEW = 'new';
    public const IN_PROGRESS = 'in progress';
    public const DONE = 'done';

    static function getPossibleStatuses()
    {
        return [
            static::NEW,
            static::IN_PROGRESS,
            static::DONE
        ];
    }
}
