<?php

namespace App\Model\Common\Car;

use Illuminate\Support\Facades\Storage;

class CarImage
{
    public const LOCATION_DIRECTORY = 'public/uploads/car-images';
    protected $name;

    public function __construct(string $imageName)
    {
        $this->name = $imageName;
    }

    public function getLink()
    {
        return str_replace(
            'public',
            'storage',
            '/' . static::LOCATION_DIRECTORY . '/' .  $this->name
        );
    }

    public function deleteFile()
    {
        Storage::delete(static::LOCATION_DIRECTORY . '/' . $this->name);
    }

}
