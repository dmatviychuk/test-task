<?php

namespace App\Model\Common\Car;

use App\Model\Common\BookingStatus;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;


class Car extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'title',
        'year',
        'rent_price',
        'color',
        'interior_color',
        'image',
    ];

    public static $storeRules = [
        'title'          => 'required|max:100',
        'year'           => 'required|integer',
        'rent_price'     => 'required|max:7',
        'color'          => 'required|max:30',
        'interior_color' => 'required|max:30',
        'image'          => 'required|image|mimes:jpeg,png,jpg,gif,svg,webp'
    ];

    public static $updateRules = [
        'title'          => 'required|max:100',
        'year'           => 'required|integer',
        'rent_price'     => 'required|max:7',
        'color'          => 'required|max:30',
        'interior_color' => 'required|max:30',
    ];

    public static function boot()
    {
        parent::boot();

        Car::deleted(function ($car) {
            $car->deleteImageFile();
        });
    }

    public function getImage(): CarImage
    {
        return new CarImage($this->image);
    }

    public function setCarImage(UploadedFile $file)
    {
        if (!empty($this->image)) {
            $this->deleteImageFile();
        }
        $fileName = uniqid() . '.' . $file->extension();
        $file->storeAs(CarImage::LOCATION_DIRECTORY, $fileName);
        $this->image = $fileName;
    }

    public function deleteImageFile()
    {
        (new CarImage($this->image))->deleteFile();
    }

    public function isRented(): bool
    {
        $result = DB::select(
            'SELECT count(*) AS number
                FROM cars 
                INNER JOIN bookings ON bookings.car_id = cars.id AND bookings.status = :status  
                WHERE cars.id  = :id',
            ['id' => $this->id, 'status' => BookingStatus::IN_PROGRESS]);
        if (!empty($result) && $result[0]->number > 0) {
            return true;
        }

        return false;
    }

}
