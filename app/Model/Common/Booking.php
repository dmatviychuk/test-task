<?php

namespace App\Model\Common;

use Illuminate\Database\Eloquent\Model;


class Booking extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'client_id',
        'start_date',
        'end_date',
        'car_id',
        'status'
    ];

    public static $storeRules = [
        'start_date'=>'required|date',
        'end_date'=>'required|date',
    ];

    public function client()
    {
        return $this->belongsTo('App\Model\Common\Client');
    }

    public function car()
    {
        return $this->belongsTo('App\Model\Common\Car\Car');
    }

    public function isHaveCar(): bool
    {
        if(!empty($this->car)){
            return true;
        }
        return false;
    }
}
