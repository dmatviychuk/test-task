@extends('admin.layouts.main')

@section('pageTitle', 'Cars List')
@section('content')

    @if(session()->has('flash.message'))
        <div class="alert alert-{{session('flash.class')}} mt-3">
            {{ session('flash.message') }}
        </div>
    @endif

    <a href="{{route('cars.create')}}" class="btn btn-success">Add Car</a>

    <table class="table table-striped mt-3">
        <thead class="table-info">
            <tr>
                <th>ID</th>
                <th>Title</th>
                <th>Year</th>
                <th>Rent Price</th>
                <th>Color</th>
                <th>Interior Color</th>
                <th>Car Status</th>
                <th>image</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            @foreach( $cars as $car)
                <tr>
                    <td>{{$car->id}}</td>
                    <td>{{$car->title}}</td>
                    <td>{{$car->year}}</td>
                    <td>{{$car->rent_price}}</td>
                    <td>{{$car->color}}</td>
                    <td>{{$car->interior_color}}</td>
                    <td>
                        @if($car->isRented())
                            <span class="text-warning">rented</span>
                        @else
                            <span class="text-success">free</span>
                        @endif
                    </td>
                    <td>
                        <img width="130px"; height="100px"
                             src="{{$car->getImage()->getLink()}}"
                        >
                    </td>
                    <td class="table-buttons">
                        <a href="{{route('cars.show', $car)}}" class="btn btn-success btn-sm">
                            <i class="fa fa-eye"></i>
                        </a>
                        <a href="{{route('cars.edit', $car)}}" class="btn btn-primary btn-sm">
                            <i class="fa fa-pen"></i>
                        </a>
                        <form method="POST" action="{{route('cars.destroy', $car)}}" >
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger btn-sm">
                                <i class="fa fa-trash"></i>
                            </button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <div class="pull-right">
        {{ $cars->links() }}
    </div>

@endsection
