@extends('admin.layouts.main')

@section('pageTitle', 'View item')

@section('content')
    <div class="col-sm-3">
        <table class="table table-striped mt-3">
            <tr>
                <td>Title</td>
                <td>{{$car->title}}</td>
            </tr>
            <tr>
                <td>Year</td>
                <td>{{$car->year}}</td>
            </tr>
            <tr>
                <td>Rent Price</td>
                <td>{{$car->rent_price}}</td>
            </tr>
            <tr>
                <td>Color</td>
                <td>{{$car->color}}</td>
            </tr>
            <tr>
                <td>Interior Color</td>
                <td>{{$car->interior_color}}</td>
            </tr>

            <tr>
                <td>Car Status</td>
                <td>
                    @if($car->isRented())
                        <span class="text-warning">rented</span>
                    @else
                        <span class="text-success">free</span>
                    @endif
                </td>
            </tr>
            <tr>
                <td>image</td>
                <td>
                    <img width="130px"; height="100px"
                         src="{{$car->getImage()->getLink()}}"
                    >
                </td>
            </tr>
        </table>
    </div>
@endsection
