@extends('admin.layouts.main')

@section('pageTitle', 'Edit Car item')

@section('content')
    <div class="row">
        <div class="col-lg-5 mx-auto">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            {!! Form::open(['route'=>['cars.update', $car->id], 'files' => true, 'method' => 'patch']) !!}
                <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text" class="form-control" id="title" name="title" value="{{old('title',$car->title)}}">
                </div>
                <div class="form-group col-sm-2">
                    <label for="year">Year</label>
                    <select class="form-control" id="year" name="year" >
                        @for( $i=2020; $i>=2010; $i--)
                            @if ($car->year == $i || old('year') == $i )
                                <option value="{{ $i }}" selected>{{ $i }}</option>
                            @else
                                <option value="{{ $i }}">{{ $i }}</option>
                            @endif
                        @endfor
                    </select>
                </div>
                <div class="form-group">
                    <label for="rent-price">Rent price</label>
                    <input type="number" class="form-control" id="rent-price" name="rent_price" value="{{old('rent_price',$car->rent_price)}}">
                </div>
                <div class="form-group">
                    <label for="color">Color</label>
                    <input type="text" class="form-control" id="color" name="color" value="{{old('color',$car->color)}}">
                </div>
                <div class="form-group">
                    <label for="interior-color">Interior Color</label>
                    <input type="text" class="form-control" id="interior-color" name="interior_color" value="{{old('interior_color',$car->interior_color)}}">
                </div>
                <div class="form-group">
                    <img width="130px"; height="100px"
                         src="{{$car->getImage()->getLink()}}"
                    >
                    <div class="mt-3">
                        <label for="image">Image: </label>
                        {!! Form::file('image') !!}
                    </div>
                </div>
                <button type="submit" class="btn btn-success">Update</button>
                {!! Form::close() !!}
        </div>
    </div>
@endsection
