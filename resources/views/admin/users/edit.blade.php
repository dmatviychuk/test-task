@extends('admin.layouts.main')

@section('pageTitle', 'Edit user')

@section('content')
<div class="row">
    <div class="col-lg-5">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form method="POST" action="{{route('users.update', $user)}}">
            @method('PATCH')
            @csrf
            <div>
                <label for="title">Title:</label> {{$user->name}}
            </div>
            <div>
                <label for="title">email:</label> {{$user->email}}
            </div>
            <div>
                <label for="year">Role:</label>
                <select class="" id="role" name="roleId" >
                    @php $currentRoleId = $user->roles->first()->id @endphp
                    @foreach($roles as $role)
                        @if ($user->roles->first()->id == $role->id)
                            <option value="{{ $role->id }}" selected>{{ $role->name }}</option>
                        @else
                            <option value="{{ $role->id }}">{{ $role->name }}</option>
                        @endif
                    @endforeach
                </select>
            </div>
            <button type="submit" class="btn btn-success mt-3">Update</button>
        </form>
    </div>
</div>

@endsection
