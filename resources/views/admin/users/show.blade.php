@extends('admin.layouts.main')

@section('pageTitle', 'View user item')

@section('content')
    <div class="col-sm-3">
        <table class="table table-striped mt-3">
            <tr>
                <td>Name</td>
                <td>{{$user->name}}</td>
            </tr>
            <tr>
                <td>Email</td>
                <td>{{$user->email}}</td>
            </tr>
            <tr>
                <td>Role</td>
                <td>{{$user->roles()->first()->name}}</td>
            </tr>
        </table>
    </div>
@endsection
