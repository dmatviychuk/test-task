@extends('admin.layouts.main')

@section('pageTitle', 'Users List')

@section('content')
    @if(session()->has('flash.message'))
        <div class="alert alert-{{session('flash.class')}} mt-3">
            {{ session('flash.message') }}
        </div>
    @endif

    <table class="table table-striped mt-3">
        <thead class="table-info">
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Email</th>
                <th>Role</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            @foreach( $users as $user)
                <tr>
                    <td>{{$user->id}}</td>
                    <td>{{$user->name}}</td>
                    <td>{{$user->email}}</td>
                    <td>{{$user->roles->first()->name}}</td>
                    <td class="table-buttons">
                        <a href="{{route('users.show', $user)}}" class="btn btn-success btn-sm">
                            <i class="fa fa-eye"></i>
                        </a>
                        <a href="{{route('users.edit', $user)}}" class="btn btn-primary btn-sm">
                            <i class="fa fa-pen"></i>
                        </a>
                        <form method="POST" action="{{route('users.destroy', $user)}}">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger btn-sm">
                                <i class="fa fa-trash"></i>
                            </button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <div class="pull-right">
        {{ $users->links() }}
    </div>

@endsection
