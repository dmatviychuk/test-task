@extends('admin.layouts.main')

@section('pageTitle', 'Add new Client')

@section('content')
    <div class="row">
        <div class="col-lg-5 mx-auto">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            {!! Form::open(['route'=>'clients.store']) !!}
            <div class="form-group">
                <label for="name">Name</label>
                <input type="text" class="form-control" id="name" name="name" value="{{old('name')}}">
            </div>
            <div class="form-group">
                <label for="phone">phone</label>
                <input type="text" class="form-control" id="phone" name="phone" value="{{old('phone')}}">
            </div>
            <button type="submit" class="btn btn-success">Save</button>
            {!! Form::close() !!}
        </div>
    </div>
@endsection
