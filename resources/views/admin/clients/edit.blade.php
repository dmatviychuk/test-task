@extends('admin.layouts.main')

@section('pageTitle', 'Edit Client item')

@section('content')
    <div class="row">
        <div class="col-lg-5 mx-auto">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            {!! Form::open(['route'=>['clients.update', $client->id],  'method' => 'patch']) !!}
            <div class="form-group">
                <label for="name">Name</label>
                <input type="text" class="form-control" id="name" name="name" value="{{old('name', $client->name)}}">
            </div>
            <div class="form-group">
                <label for="phone">phone</label>
                <input type="text" class="form-control" id="phone" name="phone" value="{{old('phone',$client->phone)}}">
            </div>
            <button type="submit" class="btn btn-success">Save</button>
            {!! Form::close() !!}
        </div>
    </div>
 @endsection
