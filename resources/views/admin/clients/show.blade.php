@extends('admin.layouts.main')

@section('pageTitle', 'View item')

@section('content')
    <div class="col-sm-3">
        <table class="table table-striped mt-3">
            <tr>
                <td>ID</td>
                <td>{{$client->id}}</td>
            </tr>
            <tr>
                <td>Name</td>
                <td>{{$client->name}}</td>
            </tr>
            <tr>
                <td>Phone</td>
                <td>{{$client->phone}}</td>
            </tr>
        </table>
    </div>
@endsection
