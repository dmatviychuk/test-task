@extends('admin.layouts.main')

@section('pageTitle', 'Clients List')
@section('content')

    @if(session()->has('flash.message'))
        <div class="alert alert-{{session('flash.class')}} mt-3">
            {{ session('flash.message') }}
        </div>
    @endif

    <a href="{{route('clients.create')}}" class="btn btn-success">Add Client</a>

    <table class="table table-striped mt-3">
        <thead class="table-info">
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Phone</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @foreach( $clients as $client)
            <tr>
                <td>{{$client->id}}</td>
                <td>{{$client->name}}</td>
                <td>{{$client->phone}}</td>
                <td class="table-buttons">
                    <a href="{{route('clients.show', $client)}}" class="btn btn-success btn-sm">
                        <i class="fa fa-eye"></i>
                    </a>
                    <a href="{{route('clients.edit', $client)}}" class="btn btn-primary btn-sm">
                        <i class="fa fa-pen"></i>
                    </a>
                    <form method="POST" action="{{route('clients.destroy', $client)}}">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger btn-sm">
                            <i class="fa fa-trash"></i>
                        </button>
                    </form>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <div class="pull-right">
        {{ $clients->links() }}
    </div>

@endsection
