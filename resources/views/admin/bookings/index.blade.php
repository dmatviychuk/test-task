@extends('admin.layouts.main')

@section('pageTitle', 'Bookings List')

@section('content')

    @if(session()->has('flash.message'))
        <div class="alert alert-{{session('flash.class')}} mt-3">
            {{ session('flash.message') }}
        </div>
    @endif

    <table class="table table-striped mt-3">
        <thead class="table-info">
            <tr>
                <th>ID</th>
                <th>status</th>
                <th>Client</th>
                <th>Start Date</th>
                <th>End Date</th>
                <th>Car</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            @foreach( $bookings as $booking)
                <tr>
                    <td>{{$booking->id}}</td>
                    <td>{{$booking->status}}</td>
                    <td><a href="{{route('clients.show', $booking->client->id)}}">{{$booking->client->name}}</a></td>
                    <td>{{$booking->start_date}}</td>
                    <td>{{$booking->end_date}}</td>
                    <td>
                        @if($booking->isHaveCar())
                            <a href="{{route('cars.show', $booking->car->id)}}"> {{$booking->car->title}}</a>
                        @endif
                    </td>

                    <td class="table-buttons">
                        <a href="{{route('bookings.show', $booking)}}" class="btn btn-success btn-sm">
                            <i class="fa fa-eye"></i>
                        </a>
                        <a href="{{route('bookings.edit', $booking)}}" class="btn btn-primary btn-sm">
                            <i class="fa fa-pen"></i>
                        </a>
                        <form method="POST" action="{{route('bookings.destroy', $booking)}}" >
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger btn-sm">
                                <i class="fa fa-trash"></i>
                            </button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <div class="pull-right">
        {{ $bookings->links() }}
    </div>

@endsection


