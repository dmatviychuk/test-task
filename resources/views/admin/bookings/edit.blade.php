@extends('admin.layouts.main')

@section('pageTitle', 'Edit Booking item')

@section('content')
<div class="row">
    <div class="col-lg-5 mx-auto">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        {!! Form::open(['route'=>['bookings.update', $booking->id], 'method' => 'patch']) !!}
        <table class="table table-striped mt-3">
            <tr>
                <td>Status</td>
                <td>
                    <select class="form-control" id="status" name="status" >
                        @foreach( $statuses as $status)
                            @if ($booking->status == $status || old('status') == $status )
                                <option value="{{ $status }}" selected>{{ $status }}</option>
                            @else
                                <option value="{{ $status }}">{{ $status }}</option>
                            @endif
                        @endforeach
                    </select>
                </td>
            </tr>
            <tr>
                <td>Start date</td>
                <td>{{$booking->start_date}}</td>
            </tr>
            <tr>
                <td>End date</td>
                <td>{{$booking->end_date}}</td>
            </tr>
            <tr>
                <td><b>Client info</b></td>
                <td></td>
            </tr>
            <tr>
                <td>Name</td>
                <td>{{$booking->client->name}}</td>
            </tr>
            <tr>
                <td>Phone</td>
                <td>{{$booking->client->phone}}</td>
            </tr>
            @if($booking->isHaveCar())
                <tr>
                    <td><b>Car info</b></td>
                    <td></td>
                </tr>
                <tr>
                    <td>Title</td>
                    <td>{{$booking->car->title}}</td>
                </tr>
                <tr>
                    <td>Year</td>
                    <td>{{$booking->car->year}}</td>
                </tr>
                <tr>
                    <td>Image</td>
                    <td>
                        <img width="130px"; height="100px"
                             src="{{$booking->car->getImage()->getLink()}}"
                        >
                    </td>
                </tr>
            @endif
        </table>
        <button type="submit" class="btn btn-success">Update</button>
        {!! Form::close() !!}
    </div>
</div>

@endsection