@extends('admin.layouts.main')

@section('pageTitle', 'View item')

@section('content')
    <div class="col-sm-3">
        <table class="table table-striped mt-3">
            <tr>
                <td>Status</td>
                <td>{{$booking->status}}</td>
            </tr>
            <tr>
                <td>Start date</td>
                <td>{{$booking->start_date}}</td>
            </tr>
            <tr>
                <td>End date</td>
                <td>{{$booking->end_date}}</td>
            </tr>
            <tr>
                <td><b>Client info</b></td>
                <td></td>
            </tr>
            <tr>
                <td>Name</td>
                <td>{{$booking->client->name}}</td>
            </tr>
            <tr>
                <td>Phone</td>
                <td>{{$booking->client->phone}}</td>
            </tr>
            @if($booking->isHaveCar())
                <tr>
                    <td><b>Car info</b></td>
                    <td></td>
                </tr>
                <tr>
                    <td>Title</td>
                    <td>{{$booking->car->title}}</td>
                </tr>
                <tr>
                    <td>Year</td>
                    <td>{{$booking->car->year}}</td>
                </tr>
                <tr>
                    <td>Image</td>
                    <td>
                        <img width="130px"; height="100px"
                             src="{{$booking->car->getImage()->getLink()}}"
                        >
                    </td>
                </tr>
            @endif
        </table>
    </div>
@endsection
