@extends('frontend.layouts.booking')

@section('content')
    <div class="card-body car-item">
        <h5 class="card-title">{{$car->title}}</h5>
        <img class="mb-3" width="310px" height="240px" src="{{$car->getImage()->getLink()}}"/>
        <p><b>Year:</b> {{$car->year}}</p>
        <p><b>Color:</b> {{$car->color}}</p>
        <p><b>Interior</b> Color: {{$car->interior_color}}</p>
        <p><b>Price:</b> $ {{$car->rent_price}}</p>
    </div>

    <div style="width: 500px;">

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        {!! Form::open(['route'=>'booking.store']) !!}
        <div class="form-group">
            <input type="hidden" class="form-control" id="car_id" name="car_id" value="{{$car->id}}">
            <label for="name">Your name:</label>
            <input type="text" class="form-control" id="name" name="name" value="{{old('name')}}">
        </div>
        <div class="form-group">
            <label for="name">Your phone:</label>
            <input type="text" class="form-control" id="phone" name="phone" value="{{old('phone')}}">
        </div>
        <div class="form-group">
            <label for="name">Start date:</label>
            <input type="date" class="form-control" id="start_date" name="start_date" value="{{old('phone')}}">
        </div>

        <div class="form-group">
            <label for="name">End date:</label>
            <input type="date" class="form-control" id="end_date" name="end_date" value="{{old('phone')}}">
        </div>
        <button type="submit" class="btn btn-success">Book it up</button>
        {!! Form::close() !!}
    </div>
@endsection
