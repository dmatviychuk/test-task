@extends('frontend.layouts.booking')

@section('content')
    <div class="row">
        <div class="card mt-5 mb-5">
            <div class="card-header">
                <h5>Success</h5>
            </div>
            <div class="card-body">
                <p class="card-title">You booked the car. Our manager contact you soon.</p>
            </div>
        </div>
    </div>
@endsection
