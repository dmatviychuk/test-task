@extends('frontend.layouts.main')

@section('content')
    <div class="card">
        <div class="card-body">
            <h5>For clients</h5>
            Simplified car search process, find a car in 1 minute.
            With 99% accuracy you will get the exact car you have requested for.
            You can always compare
            More than 80% of all rental cars in the market are available at Renty, including sports cars, luxury cars, business cars, vans, as well as chauffeur services.
            Partnering up with more than 30 reputable car rental companies in the UAE
            Each vehicle goes through the verification process before the actual publishing
            Car rental companies with Google Review below 3.5* are not listed on Renty.ae. Any existing partner, car rental company, whos score drops below 3.5* will be eliminated from our platform.
        </div>
    </div>
@endsection
