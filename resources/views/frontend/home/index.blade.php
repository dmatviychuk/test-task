@extends('frontend.layouts.main')

@section('content')
    @foreach($cars->chunk(2) as $carChunk)
        <div class="row pb-3">
            @foreach($carChunk as $car)
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-body car-item">
                            <h5 class="card-title">{{$car->title}}</h5>
                            <img class="mb-3" width="310px" height="240px" src="{{$car->getImage()->getLink()}}" />
                            <p><b>Year:</b> {{$car->year}}</p>
                            <p><b>Color:</b> {{$car->color}}</p>
                            <p><b>Interior</b> Color: {{$car->interior_color}}</p>
                            <p><b>Price:</b> $ {{$car->rent_price}}</p>
                            <a href="{{route('booking.create', $car)}}" class="btn btn-primary mt-3">Rent car</a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    @endforeach

    <div class="pull-right">
        {{ $cars->links() }}
    </div>

@endsection
