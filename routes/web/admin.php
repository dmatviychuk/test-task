<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\HomeController;
use App\Http\Controllers\Admin\User\UserController;
use App\Http\Controllers\Admin\Auth\{LoginController, RegisterController};

/*
|--------------------------------------------------------------------------
|                   ADMIN ROUTS
|--------------------------------------------------------------------------
*/

// Auth routes
Route::post('/admin/logout', [LoginController::class, 'logout'])->name('logout');
Route::match(['get', 'head'],'/admin/login', [LoginController::class, 'showLoginForm'])->name('login');
Route::post('/admin/login', [LoginController::class, 'login']);
Route::match(['get', 'head'],'/admin/register', [RegisterController::class, 'showRegistrationForm'])->name('register');
Route::post('/admin/register', [RegisterController::class, 'register']);

Route::group(
    [
        'prefix'     => 'admin',
        'namespace'  => 'Admin',
        'middleware' => ['auth']
    ], function () {
        Route::get('/', [HomeController::class, 'index'])->name('dashboard');

        Route::resource('cars', 'Car\CarController');
        Route::resource('bookings', 'BookingController');
        Route::resource('clients', 'ClientController');

        Route::get('users', [UserController::class, 'index'])->name('users.index');
        Route::match(['get', 'head'],'users/show', [UserController::class, 'show'])->name('users.show');
        Route::match(['get', 'head'],'users/{user}/edit', [UserController::class, 'edit'])->name('users.edit');
        Route::match(['put', 'patch'],'users/{user}', [UserController::class, 'update'])->name('users.update');
        Route::match(['delete'],'users/{user}', [UserController::class, 'destroy'])->name('users.destroy');
        Route::get('users/{user}', [UserController::class, 'show'])->name('users.show');
    }
);
