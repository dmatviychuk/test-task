<?php

use App\Http\Controllers\Frontend\HomeController;
use App\Http\Controllers\Frontend\BookingController;


/*
|--------------------------------------------------------------------------
|                   FRONTEND ROUTS
|--------------------------------------------------------------------------
*/

Route::group(['namespace' => 'Frontend'], function () {
    Route::get('/', [HomeController::class, 'index'])->name('home');
    Route::get('/about', [HomeController::class, 'about'])->name('about');

    Route::get('/booking/create/{car}', [BookingController::class, "create"])->name('booking.create');
    Route::post('/booking/create/store', [BookingController::class, "store"])->name('booking.store');
    Route::get('/booking/success', [BookingController::class, "success"])->name('booking.success');
});
